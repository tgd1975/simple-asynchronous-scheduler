from asyncscheduler import AsyncScheduler
from time import sleep, clock, time

a = AsyncScheduler()
a.start()
start_clock = clock()
start_time = time()

event = a.enter(1, 1, print, args=("event 1",))
a.enter(2, 1, print, args=("event 2",))
a.enter(3, 1, print, args=("event 3",))
a.enter(4, 1, print, args=("event 4",))
a.enter(99, 1, print, args=("event 99",))
a.cancel(event)
a.enterabs(time()+2.5, 1, print, args=("absevent 2.5",))
a.repeat(1.5, 1, print, args=("repeat 1.5",))

sleep(3.5)  # 3.5 to see if an empty queue leads to busy waiting (and a high clock_diff value. expected ~0.0015)

a.clear_scheduler()

a.stop()

clock_diff = clock() - start_clock  # cpu time
time_diff = time() - start_time  # execution duration
print("clock: {:.5f}, time: {:.5f}".format(clock_diff, time_diff))
