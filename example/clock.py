from asyncscheduler import AsyncScheduler
import time
import datetime
import math

start_time = time.time()
start_process_time = time.process_time()

def display_time():
    global start_time
    global start_process_time

    now = datetime.datetime.now().strftime("%H:%M:%S")
    time_diff = time.time() - start_time
    process_diff =  time.process_time() - start_process_time
    print("\r{} (time {:0.3f}, process {:0.3f})".format(now, time_diff, process_diff), end='\r')

a = AsyncScheduler()
a.start()
a.repeatabs(math.floor(time.time()) + 1, 1, 1, display_time)

try:
    while True:
        time.sleep(0.25)
except KeyboardInterrupt:
    pass

a.stop()

process_diff = time.process_time() - start_process_time  # cpu time
time_diff = time.time() - start_time  # execution duration
print("finished with clock: {:.5f}, time: {:.5f}                  ".format(process_diff, time_diff))
